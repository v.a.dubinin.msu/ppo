import numpy as np
import os

from gym import utils
from gym.envs.mujoco import MujocoEnv
from gym.spaces import Box
import math

class InvertedPendulumEnv(MujocoEnv, utils.EzPickle):
    metadata = {
        "render_modes": [
            "human",
            "rgb_array",
            "depth_array",
            "single_rgb_array",
            "single_depth_array",
        ],
        "render_fps": 25,
    }

    def __init__(self, **kwargs):
        utils.EzPickle.__init__(self, **kwargs)
        observation_space = Box(low=-np.inf, high=np.inf, shape=(4,), dtype=np.float64)
        #self.render_mode == "rgb_array"
        MujocoEnv.__init__(
            self,
            os.getcwd() + "/model.xml",
            2,
            observation_space=observation_space,
            **kwargs
        )
        self.last_ob = None

    def step(self, a):
        self.do_simulation(a, self.frame_skip)

        ob = self._get_obs()
        terminated = bool(not np.isfinite(ob).all())

        if self.render_mode == "human":
            self.render()
        # reward = 1.0
        reward = abs((ob[1] % (2 * math.pi)) - math.pi) # - 1 * (abs(ob[0]) > 0.95)
        # print(reward)
        return ob, reward, terminated, False, {}

    def reset_model(self):
        qpos = self.init_qpos
        qvel = self.init_qvel
        qpos[1] = 3.14 # Set the pole to be facing down
        self.set_state(qpos, qvel)
        return self._get_obs()

    def _get_obs(self):
        return np.concatenate([self.data.qpos, self.data.qvel]).ravel()

    def viewer_setup(self):
        assert self.viewer is not None
        v = self.viewer
        v.cam.trackbodyid = 0
        v.cam.distance = self.model.stat.extent
