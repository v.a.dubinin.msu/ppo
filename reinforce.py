import numpy as np
from itertools import chain
import torch
import wandb

from nets import PolicyModel, ValueModel


# def get_actions_policy(action, policy):
#     action = torch.where(action == -2.5, 0, 1)
#     return policy[torch.arange(policy.shape[0]), action]


class Agent:
    def __init__(self, config):
        self.config = config
        self.policy_model = PolicyModel(config)
        self.value_model = ValueModel()
        params_gen = chain(self.policy_model.parameters(), self.value_model.parameters())
        self.optimizer = torch.optim.Adam(params_gen, lr=config['learning_rate']) # , weight_decay=3e-4)
        self.minibatches_count = config['minibatches_count']
        self.learn_count = 0

    def get_action(self, obs, actions=None):
        obs_count = np.shape(obs)[0]
        policy_mu, policy_sigma = self.policy_model(obs) # map(list, zip(*self.policy_model(obs)))

        normal_distr = torch.distributions.normal.Normal(policy_mu,
                                                        torch.exp(policy_sigma))
        if actions is None:
            actions = normal_distr.sample()
        # actions = torch.clip(actions, -3, 3)
        log_probs = normal_distr.log_prob(actions)
        mean_mu = torch.mean(policy_mu).item()
        mean_sigma = torch.mean(torch.exp(policy_sigma)).item()
        # print("Mu:", policy_mu.item())
        # print("Sigma:", policy_sigma.item())
        # print("Action:", actions.item())
        # print("Log probs:", log_probs.item())
        # print("Numpy log probs:", np.log(scipy.stats.norm(policy_mu.item(), np.exp(policy_sigma.item())).pdf(actions.item())))
        # print()

        value = self.value_model(obs)
        return torch.clip(actions, -3, 3), log_probs, (mean_mu, mean_sigma), value

        # policy = torch.softmax(self.policy_model(obs), dim=1)
        # actions = [np.random.choice([-2.5, 2.5], p=policy[i].detach().numpy()) for i in range(obs_count)]
        # # actions = np.random.normal(policy_mu, policy_sigma)
        # return actions, policy # np.clip(actions, -3, 3)

    def learn(self, trajectories):
        self.learn_count += 1
        self.optimizer.zero_grad()
        envs_count = trajectories[0][0].shape[0]
        sum_reward = torch.zeros(envs_count)
        gamma = self.config['gamma']
        value_coef = self.config['value_coef']
        policy_loss = 0
        value_loss = 0
        value_diff = 0
        next_value = self.value_model(torch.Tensor(trajectories[-1][-1]))  # next_obs
        value_mean = 0
        for obs, action, reward, done, policy, value, next_obs in trajectories[::-1]:
            next_value = next_value * (1 - done)
            with torch.no_grad():
                advantage = reward + next_value - value
                value_target = advantage + value
            value_loss += torch.mean((value - value_target) ** 2)
            value_diff += torch.mean(torch.abs(value - value_target) / torch.maximum(torch.abs(value), torch.abs(value_target)))
            value_mean += torch.mean(torch.abs(value))
            sum_reward = sum_reward * (1 - done)
            sum_reward = sum_reward * gamma + reward
            policy_loss += torch.mean(-policy * advantage)
            # loss += torch.mean(-torch.log(get_actions_policy(action, policy)) * sum_reward)
            next_value = value
        print()
        print("Mean sum reward in learner:", torch.mean(sum_reward).item())
        print("Mean value:", value_mean / len(trajectories))
        print("Mean value diff:", value_diff / len(trajectories))
        loss = policy_loss + value_coef * value_loss
        loss.backward()
        self.optimizer.step()
        return policy_loss.item(), value_loss.item()

    def rollout(self, trajectories, use_GAE=True):
        train_data = []
        next_value = self.value_model(torch.Tensor(trajectories[-1][-1]))  # next_obs
        gamma = self.config['gamma']
        td_lambda = self.config['td_lambda']
        sum_reward = 0
        advantage = 0
        mean_reward = 0
        for obs, action, reward, done, policy, value, next_obs in trajectories[::-1]:
            next_value = next_value * (1 - done)
            sum_reward = sum_reward * gamma + reward
            mean_reward += torch.mean(reward)
            with torch.no_grad():
                if use_GAE:
                    delta = reward + gamma * next_value - value
                    advantage = delta + gamma * td_lambda * advantage * (1 - done)
                    value_target = advantage + value
                else:
                    advantage = reward + gamma * next_value - value
                    value_target = advantage + value
            train_data.append([obs, action, done, policy, value, advantage, value_target])
            next_value = value

        # Ugly advantage normalization
        advs_t = torch.stack([elem[-2] for elem in train_data])
        advs_mean, advs_std = advs_t.mean(), advs_t.std()
        for train_idx, elem in enumerate(train_data):
            train_data[train_idx][-2] = (elem[-2] - advs_mean) / (advs_std + 1e-8)

        return train_data, torch.mean(sum_reward).item(), mean_reward.item() / len(train_data)

    def minibatch_learn(self, trajectories):
        self.learn_count += 1
        rollout_trajectories, total_rollout_reward, mean_rollout_reward = self.rollout(trajectories)
        indices = np.arange(len(rollout_trajectories))
        np.random.shuffle(indices)
        assert len(rollout_trajectories) % self.minibatches_count == 0
        minibatch_size = len(rollout_trajectories) // self.minibatches_count

        value_mean = 0
        value_diff = 0

        for start_idx in np.arange(0, len(rollout_trajectories), minibatch_size):
            self.optimizer.zero_grad()
            value_loss = 0
            policy_loss = 0
            value_coef = self.config['value_coef']
            clip_eps = self.config['clip_eps']
            for batch_idx in np.arange(start_idx, start_idx + minibatch_size):
                current_idx = indices[batch_idx]
                obs, action, done, old_policy, old_value, advantage, value_target = rollout_trajectories[current_idx]
                _, new_logprobs, _, new_value = self.get_action(obs, action)
                policy_ratio = torch.exp(new_logprobs - old_policy.detach())
                clipped_ratio = torch.clip(policy_ratio, 1 - clip_eps, 1 + clip_eps)
                policy_loss += -torch.mean(torch.minimum(advantage * policy_ratio, advantage * clipped_ratio))
                value_loss += torch.mean((new_value - value_target) ** 2)

                value_diff += torch.mean(torch.abs(new_value.detach() - value_target) / torch.maximum(torch.abs(new_value.detach()), torch.abs(value_target)))
                value_mean += torch.mean(torch.abs(new_value.detach()))

            policy_loss /= minibatch_size
            value_loss /= minibatch_size
            final_loss = policy_loss + value_coef * value_loss
            final_loss.backward()
            self.optimizer.step()

        wandb.log({'Rollout reward': total_rollout_reward,
                   'Mean rollout reward': mean_rollout_reward,
                   'Mean value': value_mean / len(trajectories),
                   'Mean value diff': value_diff / len(trajectories),
                   'Total loss': policy_loss.item() + value_coef * value_loss.item()})

        print()
        print("Total sum reward in learner:", total_rollout_reward)
        print("Mean sum reward in learner:", mean_rollout_reward)
        print("Mean value:", value_mean / len(trajectories))
        print("Mean value diff:", value_diff / len(trajectories))

        return policy_loss.item(), value_loss.item()
