import torch
import wandb
import os


class PolicyModel(torch.nn.Module):
    def __init__(self, config):
        super(PolicyModel, self).__init__()
        self.linear1 = torch.nn.Linear(4, 16)
        self.activation1 = torch.nn.ReLU()
        self.linear2 = torch.nn.Linear(16, 16)
        self.activation2 = torch.nn.ReLU()
        self.linear3 = torch.nn.Linear(16, 1)
        self.logsigma = config['logsigma']

    def forward(self, x):
        x = self.linear1(x)
        x = self.activation1(x)
        x = self.linear2(x)
        x = self.activation2(x)
        x = self.linear3(x)
        mu = x[:, 0]
        sigma = torch.Tensor([self.logsigma]) # x[:, 1]
        return mu, sigma


class ValueModel(torch.nn.Module):
    def __init__(self):
        super(ValueModel, self).__init__()
        self.linear1 = torch.nn.Linear(4, 16)
        self.activation1 = torch.nn.ReLU()
        self.linear2 = torch.nn.Linear(16, 16)
        self.activation2 = torch.nn.ReLU()
        self.linear3 = torch.nn.Linear(16, 1)

    def forward(self, x):
        x = self.linear1(x)
        x = self.activation1(x)
        x = self.linear2(x)
        x = self.activation2(x)
        value = self.linear3(x)
        return value


def log_model(model, model_name, model_version):
    dirpath = "/".join([wandb.run.dir, "checkpoints", model_name])
    filename = '_'.join([model_name, str(model_version)]) + ".pth"
    os.makedirs(dirpath, exist_ok=True)
    torch.save(model.state_dict(), '/'.join([dirpath, filename]))

    # ➕ another way to add a file to an Artifact
    # model_artifact.add_file(filepath)

    # wandb.save('/'.join([dirpath, filename]))

    # run.log_artifact(model_artifact)


def log_model_artifact(run, model, model_name, model_version):
    model_artifact = wandb.Artifact(
        model_name, type="model",
        description=model_name)

    filepath = "/".join(["checkpoints", model_name, '_'.join([model_name, str(model_version)]) + ".pth"])
    torch.save(model.state_dict(), filepath)

    # ➕ another way to add a file to an Artifact
    model_artifact.add_file(filepath)

    # wandb.save(filepath)

    run.log_artifact(model_artifact)
