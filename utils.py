import time


class Timer:
    def __init__(self, message_text):
        self.total_time = 0
        self.current_time = None
        self.message = message_text

    def start(self):
        self.current_time = time.time()

    def stop(self):
        self.total_time += time.time() - self.current_time
        self.current_time = None

    def __str__(self):
        return self.message + ": " + str(self.total_time)

    def reset(self):
        self.total_time = 0
        self.current_time = None
