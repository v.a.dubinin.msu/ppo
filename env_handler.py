import numpy as np
from joblib import Parallel, delayed


class EnvironmentsHandler:
    def __init__(self, environments_count, env_constructor, *env_args, **env_kwargs):
        self.envs_count = environments_count
        self.envs = [env_constructor(*env_args, **env_kwargs) for _ in range(self.envs_count)]

    def step(self, actions):
        actions_count = np.shape(actions)[0]
        assert actions_count == self.envs_count, "ERROR: actions count is {0}, while Envs has {1} environments".format(actions_count, self.envs_count)

        # step_list = Parallel(n_jobs=8)(delayed(lambda x: env.step(x))([actions[idx]]) for idx, env in enumerate(self.envs))
        step_list = [env.step([actions[idx]]) for idx, env in enumerate(self.envs)]

        next_obs, reward, next_done, next_truncated, info = map(list, zip(*step_list))
        next_done = next_done or next_truncated

        for env_idx, (done, env) in enumerate(zip(next_done, self.envs)):
            if done:
                next_obs[env_idx] = env.reset()[0]
                print("DONE WAS OBTAINED!")

        return np.array(next_obs), np.array(reward), np.array(next_done), info

    def reset(self):
        next_obs = np.stack([env.reset()[0] for env in self.envs], axis=0, dtype=np.float32)
        return next_obs
