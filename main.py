import tqdm
import numpy as np
import torch

from env_handler import EnvironmentsHandler
from inverted_pendulum_env import InvertedPendulumEnv
from reinforce import Agent
from utils import Timer

from nets import log_model

import wandb
wandb.login()

import os
os.environ["WANDB_MODE"] = "offline"

config = {
    'logsigma': -0.75,
    'envs_count': 96,
    'learning_rate': 3e-4,
    'minibatches_count': 4,
    'value_coef': 1e-4,
    'clip_eps': 0.2,
    'gamma': 0.99,
    'td_lambda': 0.95,
    'trajectory_length': 256,
    'epochs_count': 10000,
    'model_log_step': 10
}

envs_count = config['envs_count']

import matplotlib.pyplot as plt

# env = InvertedPendulumEnv(render_mode="single_rgb_array")
# env.reset_model()
# plt.imshow(env.render())

envs = EnvironmentsHandler(envs_count, InvertedPendulumEnv, render_mode="single_rgb_array")  # TODO why no

agent = Agent(config)
trajectory_length = config['trajectory_length']

losses = []
total_rewards = []

# obs = envs.reset()
# next_done = np.zeros(envs_count)

config['agent_learn_count'] = agent.learn_count

run = wandb.init(
    project="Pendulum - PPO",
    config=config
)

obs = envs.reset()

action_timer = Timer("Get action time")
step_timer = Timer("Env step time")
learn_timer = Timer("Learn time")
gen_timer = Timer("Generation time")

for update in tqdm.trange(config['epochs_count']):
    data = []
    mus = []
    gen_timer.start()
    for step in range(trajectory_length):
        t_obs = torch.Tensor(obs)
        action_timer.start()
        action, policy, (mean_mu, mean_sigma), value = agent.get_action(t_obs)
        action_timer.stop()
        mus.append(mean_mu)
        step_timer.start()
        next_obs, reward, done, info = envs.step(
            action
        ) # step in N environments
        step_timer.stop()
        data.append((t_obs, torch.Tensor(action), torch.Tensor(reward),
                     torch.Tensor(done), policy, value, next_obs))
        obs = next_obs

        if done.any():
            print("$" * 150 + " DONE HAS BEEN FOUND!")
    # agent.minibatch_learn(data)
    gen_timer.stop()

    learn_timer.start()
    losses.append(agent.minibatch_learn(data))
    learn_timer.stop()
    print()
    print("Mu:", np.mean(mus))
    print("Policy loss:", losses[-1][0])
    print("Value loss:", losses[-1][1])
    wandb.log({'Mu': np.mean(mus),
               'Policy loss': losses[-1][0],
               'Value loss': losses[-1][1]})
    if update == 1:
        print("~" * 50)
        print(action_timer)
        print(step_timer)
        print(gen_timer)
        print(learn_timer)
        print("~" * 50)

    if (update + 1) % config['model_log_step'] == 0:
        log_model(agent.policy_model, 'policy_model', (update + 1))
        log_model(agent.value_model, 'value_model', (update + 1))

    print()

wandb.finish()
